package com.example.taller2

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_inicio.*
import kotlin.math.sign

enum class ProviderType{
    BASIC,
    GOOGLE,
    FACEBOOK
}
class InicioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio)

        val bundle: Bundle? = intent.extras
        val email: String? = bundle?.getString("email")
        val proveedor: String? = bundle?.getString("proveedor")
        setup(email ?: "", proveedor ?: "")

        // INGRESA DATOS

        val prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        prefs.putString("email", email)
        prefs.putString("provider", proveedor)
        prefs.apply()
    }

    private fun setup(email: String, proveedor: String){

        title = "Inicio"
        emailtextView.text = email
        proveedortextView2.text = proveedor

        singbutton.setOnClickListener{
            val prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
            prefs.clear()
            prefs.apply()

            if(proveedor == ProviderType.FACEBOOK.name){
                LoginManager.getInstance().logOut()
            }

            FirebaseAuth.getInstance().signOut()
            onBackPressed()
        }

    }
}
